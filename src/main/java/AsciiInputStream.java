import java.io.*;

public class AsciiInputStream extends FileInputStream {



    protected AsciiInputStream(File in) throws FileNotFoundException {
        super(in);
    }
    public int countLines(File file) throws IOException {
        try (AsciiInputStream stream = new AsciiInputStream(file)) {
            byte[] b = stream.readAllBytes();
            int count = 1;
            for (byte i : b) {
                if (i == 10) {
                    count++;
                }
            }
            return count;
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File not found: " + e);
        } catch (IOException e) {
            throw new IOException("Fehler" + e);
        }
    }
    public String readLine() throws IOException {
        StringBuilder sb = new StringBuilder();
        int b = -1;
        while (true) {
            b = this.read();
            if (b == -1) {
                return sb.toString();
            }
            if (b == '\n') {
                return sb.toString();
            }
            if (b != '\r') {
                sb.append((char) b);
            }
        }
    }
}


