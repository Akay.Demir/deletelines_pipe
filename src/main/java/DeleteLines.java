import java.io.*;
import java.util.Set;
import java.util.TreeSet;

public class DeleteLines {
private Set<Integer> set= new TreeSet<>();
    public  void delete(String[] strings) {
        File inputFilename = new File(strings[0]);
        File outputFilename = new File(strings[1]);
        a(strings);

        try (AsciiInputStream asciiInputStream = new AsciiInputStream(inputFilename); FileOutputStream fileOutputStream = new FileOutputStream(outputFilename)) {

            for (int i = 0; i <   asciiInputStream.countLines(inputFilename); i++) {
                if(set.contains(i)){
                    asciiInputStream.readLine();

                }else{
                    fileOutputStream.write(asciiInputStream.readLine().getBytes());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    public  void a (String[] strings){

        for (int i = 2; i < strings.length; i++) {
            String args = strings[i];
            if(args.contains("-")){
                String von = args.split("-") [0];
                String bis = args.split("-") [1];
                for (int j =  Integer.parseInt(von) ; j <= Integer.parseInt(bis) ; j++) {
                    this.set.add(j);
                }
            }else{
                this.set.add(Integer.parseInt(args));
            }
        }
    }


    public static void main(String[] args) {
        DeleteLines deleteLines = new DeleteLines();
        deleteLines.delete(args);

    }
}


